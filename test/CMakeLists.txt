add_executable(test
	main.cpp
	tests.cpp
)


target_link_libraries(test PRIVATE rdi_buckwalter)

# get catch2.
include(${cmake_scripts_path}/rdi_prebuilt.cmake)
rdi_prebuilt_include(test PRIVATE "catch2" ${workspace_location})
